#!/bin/bash

file="data.txt"
url="http://localhost:5000"

lines=($(cat $file))

for line in "${lines[@]}"; do

    if [[ $line =~ 'name{"' ]]; then
        name=$(echo $line | cut -d '"' -f2)
    elif [[ $line =~ 'email{"' ]]; then
        email=$(echo $line | cut -d '"' -f2)
    elif [[ $line =~ 'code{"' ]]; then
        code=$(echo $line | cut -d '"' -f2)
    fi

    if [ -n "$email" ] && [ -n "$code" ] && [ -n "$name" ]; then
        json=$(cat <<EOF
{ 
    "email": "$email",
    "shortname": "$code",
    "fullname": "$name"
}
EOF
)
        curl -X POST -H "Content-Type: application/json" -d "$json" "$url"
        echo "$json"
        name=""
        email=""
        code=""
    fi
    
done

# TODO: parse data.txt and add to database

# The JSON format in which you need to post the data should look like this:
# { 
#     "email": "example@example.com",
#     "shortname": "abc01",
#     "fullname": "name of person"
# }
