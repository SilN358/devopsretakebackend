#!/bin/bash
#
#  Usage:
#  build - Create docker container(s) for the application
#  run   - Run the application as container(s)
#  stop  - Stop the container(s)

if [ "$1" == "build" ]; then
  # Build images
  docker-compose build
elif [ "$1" == "run" ]; then
  # Start containers
  docker-compose up
elif [ "$1" == "stop" ]; then
  # Stop containers
  docker-compose down
else
  echo "Gebruik: $0 { build | run | stop }"
fi